﻿package main

import (
	"log"

	"github.com/night-codes/tokay"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type obj map[string]interface{}

func main() {
	session, err := mgo.Dial(":27017") //mongodb connect
	if err != nil {
		panic(err)
	}

	r := tokay.New()
	r.GET("/ping", func(c *tokay.Context) {
		c.String(200, "pong")
	})
	DBuser := session.DB("NewDB").C("Users")
	DBgroup := session.DB("NewDB").C("Group")

	//Method - create, Collection - Users, Params - login, pass, age.
	r.GET("/api/user/create", func(c *tokay.Context) {
		login, pass, age := c.Query("login"), c.Query("pass"), c.Query("age")
		valueforsavemap := obj{"login": login, "pass": pass, "age": age}
		err := DBuser.Insert(valueforsavemap)
		if err != nil {
			panic(err)
		}
		retval := obj{}
		err = DBuser.Find(valueforsavemap).Select(obj{"_id": true}).One(&retval)
		if err != nil {
			panic(err)
		}
		c.JSON(200, obj{
			"_id": retval,
		})
	})

	//Method - delete, Collection - Users, Params - _id
	r.GET("/api/user/delete", func(c *tokay.Context) {
		id := c.Query("id")
		tmp := bson.ObjectIdHex(id)
		err = DBuser.Find(obj{"_id": tmp}).One(obj{})
		log.Println(tmp)
		if err != nil {
			c.JSON(200, obj{
				"message": "Id doesnt exists!",
			})
		} else {
			err = DBuser.Remove(obj{"_id": tmp})
			if err != nil {
				panic(err)
			} else {
				c.JSON(200, obj{
					"ok": "true",
				})
			}
		}
	})

	//Method - findLogin, Collection - Users, Params - login
	r.GET("/api/user/findLogin", func(c *tokay.Context) {
		login := c.Query("login")
		retmapID := []obj{}
		err = DBuser.Find(obj{"login": login}).Select(obj{"_id": true}).One(obj{})
		if err != nil {
			c.JSON(200, obj{
				"message": "Login doesnt exists!",
			})
		} else {
			err = DBuser.Find(obj{"login": login}).Select(obj{"_id": true}).All(&retmapID)
			c.JSON(200, obj{
				"_id": retmapID,
			})
		}
	})

	r.GET("/api/group/create", func(c *tokay.Context) {
		tittle := c.Query("tittle")
		savemap := obj{"tittle": tittle}
		err := DBgroup.Insert(savemap)
		if err != nil {
			panic(err)
		}
		retval := obj{}
		err = DBgroup.Find(savemap).Select(obj{"_id": true}).One(&retval)
		if err != nil {
			panic(err)
		}
		c.JSON(200, obj{
			"_id": retval,
		})
	})

	r.GET("/api/group/delete", func(c *tokay.Context) {
		id := c.Query("id")
		tmp := bson.ObjectIdHex(id)
		err = DBgroup.Find(obj{"_id": tmp}).One(obj{})
		log.Println(tmp)
		if err != nil {
			c.JSON(200, obj{
				"message": "Id doesnt exists!",
			})
		} else {
			err = DBgroup.Remove(obj{"_id": tmp})
			if err != nil {
				panic(err)
			} else {
				c.JSON(200, obj{
					"ok": "true",
				})
			}
		}
	})

	r.GET("/api/group/addUser", func(c *tokay.Context) {
		grID, userID := c.Query("id"), c.Query("userId")
		if bson.IsObjectIdHex(grID) != false && bson.IsObjectIdHex(userID) != false {
			newgrID, newuserID := bson.ObjectIdHex(grID), bson.ObjectIdHex(userID)
			err = DBgroup.Find(obj{"_id": newgrID}).One(obj{})
			if err != nil {
				c.JSON(200, obj{
					"message": "group doesent exist",
				})
			} else {
				err = DBuser.Find(obj{"_id": newuserID}).Select(obj{"_id": 1}).One(obj{})
				if err != nil {
					c.JSON(200, obj{
						"message": " userID doesnt exists!",
					})
				} else {
					err = DBuser.UpdateId(newuserID, obj{"$push": obj{"groupID": grID}})
					if err != nil {
						panic(err)
					} else {
						c.JSON(200, obj{
							"ok": "true",
						})
					}
				}
			}
		} else {
			c.JSON(200, obj{
				"message": "invalid query param",
			})
		}
	})

	r.GET("/api/group/getUsers", func(c *tokay.Context) {
		id := c.Query("id")
		if bson.IsObjectIdHex(id) != false {
			groupID := bson.ObjectIdHex(id)
			err = DBgroup.Find(obj{"_id": groupID}).One(obj{})
			if err != nil {
				c.JSON(200, obj{
					"message": "Id doesnt exists!",
				})
			} else {
				userInfo := []obj{}
				returnInfo := []obj{}
				err = DBuser.Find(obj{"groupID": obj{"$exists": 1}}).All(&userInfo)
				if len(userInfo) == 0 {
					c.JSON(200, obj{
						"message": "All of Users dont have groups!",
					})
				} else {
					userCounter := 0
					for index := range userInfo {
						mapvalue := userInfo[index]["groupID"].([]interface{})
						for idArray := range mapvalue {
							if mapvalue[idArray] == id {
								tmpInfo := obj{}
								err = DBuser.Find(obj{"_id": userInfo[index]["_id"]}).Select(obj{"groupID": 0}).One(&tmpInfo)
								returnInfo = append(returnInfo, tmpInfo)
								userCounter++
							}
						}
					}
					if userCounter == 0 {
						c.JSON(200, obj{
							"message": "This group havent users!",
						})
					} else {
						c.JSON(200, returnInfo)
					}
				}
			}
		} else {
			c.JSON(200, obj{
				"message": "invalid query param",
			})
		}
	})

	//Method - deleteUser, Collection - Groups, Params - id, userID
	r.GET("/api/group/deleteUser", func(c *tokay.Context) {
		groupID, userID := c.Query("id"), c.Query("userID")
		if bson.IsObjectIdHex(groupID) != false && bson.IsObjectIdHex(userID) != false {
			newGrID, newUsID := bson.ObjectIdHex(groupID), bson.ObjectIdHex(userID)
			err = DBgroup.Find(obj{"_id": newGrID}).One(obj{})
			if err != nil {
				c.JSON(200, obj{
					"message": "This groupID doesnt exists!",
				})
			} else {
				testvalue := obj{}
				err = DBuser.Find(obj{"_id": newUsID}).One(&testvalue)
				if err != nil {
					c.JSON(200, obj{
						"message": "This userID doesnt exists!",
					})
				} else {
					tmpValue := testvalue["groupID"].([]interface{})
					groupCounter := 0
					for indexValue := range tmpValue {
						if tmpValue[indexValue] == groupID {
							groupCounter++
							err = DBuser.UpdateId(newUsID, obj{"$pop": obj{"groupID": newGrID}})
							if err != nil {
								panic(err)
							} else {
								c.JSON(200, obj{
									"ok": "true",
								})
							}
						}
					}
					if groupCounter == 0 {
						c.JSON(200, obj{
							"message": "This userID doesnt exists in groupID!",
						})
					}
				}
			}
		} else {
			c.JSON(200, obj{
				"message": "invalid query param",
			})
		}
	})

	//Method - plus, Collection - Groups, Params - id1, id2
	r.GET("/api/group/plus", func(c *tokay.Context) {
		groupID1, groupID2 := c.Query("id1"), c.Query("id2")
		if bson.IsObjectIdHex(groupID1) != false && bson.IsObjectIdHex(groupID2) != false {
			gr1, gr2 := bson.ObjectIdHex(groupID1), bson.ObjectIdHex(groupID2)
			err = DBgroup.Find(obj{"_id": gr1}).One(obj{})
			if err != nil {
				c.JSON(200, obj{
					"message": "id1 doesnt exists!",
				})
			} else {
				err = DBgroup.Find(obj{"_id": gr2}).One(obj{})
				if err != nil {
					c.JSON(200, obj{
						"message": "id2 doesnt exists!",
					})
				} else {
					infoID := []obj{}
					usersAdd := []obj{}
					err = DBuser.Find(obj{"groupID": obj{"$exists": 1}}).Select(obj{"groupID": 1}).All(&infoID)
					for index := range infoID {
						tmpVal := infoID[index]["groupID"].([]interface{})
						existsCheck1, existsCheck2 := 0, 0
						for grIndex := range tmpVal {
							if tmpVal[grIndex] == groupID1 {
								existsCheck1++
							}
							if tmpVal[grIndex] == groupID2 {
								existsCheck2++
							}
						}
						if existsCheck1 == 0 && existsCheck2 == 1 {
							usersAdd = append(usersAdd, infoID[index])
						}
					}
					if len(usersAdd) == 0 {
						c.JSON(200, obj{
							"message": "No users to add",
						})
					} else {
						for retIndex := range usersAdd {
							retUserID := usersAdd[retIndex]["_id"]
							err = DBuser.UpdateId(retUserID, obj{"$push": obj{"groupID": groupID1}})
							if err != nil {
								panic(err)
							}
						}
						c.JSON(200, obj{
							"ok": "true",
						})
					}
				}
			}
		} else {
			c.JSON(200, obj{
				"message": "invalid query param",
			})
		}
	})

	//Method - minus, Collection - Groups, Params - id1, id2 // удалить из первой группы юзеров второй группы
	r.GET("/api/group/minus", func(c *tokay.Context) {
		groupID1, groupID2 := c.Query("id1"), c.Query("id2")
		if bson.IsObjectIdHex(groupID1) != false && bson.IsObjectIdHex(groupID2) != false {
			gr1, gr2 := bson.ObjectIdHex(groupID1), bson.ObjectIdHex(groupID2)
			err = DBgroup.Find(obj{"_id": gr1}).One(obj{})
			if err != nil {
				c.JSON(200, obj{
					"message": "id1 doesnt exists!",
				})
			} else {
				err = DBgroup.Find(obj{"_id": gr2}).One(obj{})
				if err != nil {
					c.JSON(200, obj{
						"message": "id2 doesnt exists!",
					})
				} else {
					infoID := []obj{}
					usersDel := []obj{}
					err = DBuser.Find(obj{"groupID": obj{"$exists": 1}}).Select(obj{"groupID": 1}).All(&infoID)
					for index := range infoID {
						tmpVal := infoID[index]["groupID"].([]interface{})
						existsCheck1, existsCheck2 := 0, 0
						for grIndex := range tmpVal {
							if tmpVal[grIndex] == groupID1 {
								existsCheck1++
							}
							if tmpVal[grIndex] == groupID2 {
								existsCheck2++
							}
						}
						if existsCheck1 == 1 && existsCheck2 == 1 {
							usersDel = append(usersDel, infoID[index])
						}
					}
					if len(usersDel) == 0 {
						c.JSON(200, obj{
							"message": "No users to delete",
						})
					} else {
						for retIndex := range usersDel {
							retUserID := usersDel[retIndex]["_id"]
							err = DBuser.UpdateId(retUserID, obj{"$pop": obj{"groupID": groupID1}})
							if err != nil {
								panic(err)
							}
						}
						c.JSON(200, obj{
							"ok": "true",
						})
					}
				}
			}
		} else {
			c.JSON(200, obj{
				"message": "invalid query param",
			})
		}
	})
	panic(r.Run(":4000", "Application started at http://localhost%s"))

}
